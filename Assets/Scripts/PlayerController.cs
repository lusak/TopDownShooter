﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 0.1f;

    private Camera camera;
    private Vector3 mousePos;
    private Vector3 movement;
    private bool moved = false;
    private bool active = true;

    private Rigidbody2D rBody;
    private Health health;
    private LevelManager levelManager;
    private HealthBar healthBar;


    private void Awake()
    {
        camera = Camera.main;
        healthBar = FindObjectOfType<HealthBar>();
        levelManager = FindObjectOfType<LevelManager>();
        rBody = GetComponent<Rigidbody2D>();
        health = GetComponent<Health>();

        health.onDeathEvent += OnDeath;

        PauseMenu.instance.onGamePaused += OnGamePaused;
        PauseMenu.instance.onGameUnpaused += OnGameUnpaused;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (enemy)
        {
            health.TakeDamage(enemy.Damage);
            healthBar.TakeDamage(enemy.Damage);
            enemy.OnDeath();
        }
    }

    void Update()
    {
        if(active)
        {
            RotateTowardsMouse();
            Move();
        }
    }

    private void FixedUpdate()
    {
        if (moved)
        {
            rBody.MovePosition(transform.position + movement * moveSpeed);
            moved = false;
        }
    }

    private void OnDestroy()
    {
        PauseMenu.instance.onGamePaused -= OnGamePaused;
        PauseMenu.instance.onGameUnpaused -= OnGameUnpaused;
        health.onDeathEvent -= OnDeath;
    }

    private void OnDeath() => levelManager.GameOver();

    private void OnGamePaused() => active = false;

    private void OnGameUnpaused() => active = true;

    private void Move()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        if (x != 0 || y != 0)
        {
            movement = new Vector3(x, y, 0);
            moved = true;
        }
    }

    private void RotateTowardsMouse()
    {
        mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = new Vector2(mousePos.x - transform.position.x, mousePos.y - transform.position.y);
        transform.up = direction;
    }

}
