﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupsSpawner : MonoBehaviour
{
    /*Values of below props are smaller than playableArea, because I don't want..
    ..them to spawn at the edges*/

    //Width of the playable area in world units - 1
    [SerializeField] private int xPosOfPlayableArea = 8;
    //Width of the playable area in world units - 1
    [SerializeField] private int yPosOfPlayableArea = 4;
    [SerializeField] private float timeBetweenSpawns = 5;

    //Object to help keep hierarchy clean
    [SerializeField] private GameObject parentObjectOfPickups;

    //I don't want too many pickups at the screen
    [SerializeField] private int maxNumberOfActivePickups = 5;

    [SerializeField] private Pickup[] availablePickupTypes;

    public static int NumberOfActivePickups { get; set; }

    public bool Active { get; set; }


    void Start()
    {
        if(availablePickupTypes == null)
        {
            Debug.LogError("No pickup prefab declared for pickup spawner");
        } else
        {
            Active = true;
            StartCoroutine(SpawnPickups());
        }
    }

    private IEnumerator SpawnPickups()
    {
        while(Active)
        {
            yield return new WaitForSeconds(timeBetweenSpawns);
            if (NumberOfActivePickups<maxNumberOfActivePickups)
            {
                Instantiate(availablePickupTypes[Random.Range(0, availablePickupTypes.Length)], 
                    PickRandomSpawnPosition(), Quaternion.identity, parentObjectOfPickups.transform);
                NumberOfActivePickups++;
            }
        }
    }

    private Vector2 PickRandomSpawnPosition()
    {
        float spawnXPosition = Random.Range(-xPosOfPlayableArea, xPosOfPlayableArea);
        float spawnYPosition = Random.Range(-yPosOfPlayableArea, yPosOfPlayableArea);

        return new Vector2(spawnXPosition, spawnYPosition);
    }
}
