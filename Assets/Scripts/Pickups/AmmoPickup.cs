﻿using UnityEngine;
using System.Collections;

public class AmmoPickup : Pickup
{
    [SerializeField] private int minQuantOfAmmo = 10;
    [SerializeField] private int maxQuantOfAmmo = 26;

    public override void GrantBonus(Gun gun)
    {
        gun.ReceiveAmmo(Random.Range(minQuantOfAmmo, maxQuantOfAmmo));
    }
}