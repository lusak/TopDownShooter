﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    [SerializeField] private float explosionRadious = 1.5f;
    [SerializeField] private int explosionDamage = 100;
    [SerializeField] private float explosionDuration = 0.4f;
    [SerializeField] private AudioClip explodeClip;

    private Health health;

    private void Start()
    {
        health = GetComponent<Health>();
        health.onDeathEvent += OnDeath;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<PlayerController>())
        {
            Gun gun = FindObjectOfType<Gun>();
            if(gun)
            {
                GrantBonus(gun);
            }
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        PickupsSpawner.NumberOfActivePickups--;
        health.onDeathEvent -= OnDeath;
    }

    public abstract void GrantBonus(Gun gun);

    private void OnDeath()
    {
        StartCoroutine(Explode());
    }

    //Explosion does not hurt player or other pickups
    private IEnumerator Explode()
    {
        LayerMask mask = LayerMask.GetMask("Enemies");
        Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, explosionRadious, mask);
        foreach (Collider2D enemy in enemies)
        {
            enemy.gameObject.GetComponent<Health>().TakeDamage(explosionDamage);
        }
        GetComponent<Animator>().SetTrigger("explode");
        SoundManager.instance.PlayAudio(explodeClip, 1f);
        yield return new WaitForSeconds(explosionDuration);
        Destroy(gameObject);

    }
}
