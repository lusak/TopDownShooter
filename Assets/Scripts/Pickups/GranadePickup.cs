﻿using UnityEngine;
using System.Collections;

public class GranadePickup : Pickup
{
    [SerializeField] private int minGranadesNumber = 1;
    [SerializeField] private int maxGranadesNumber = 4;

    public override void GrantBonus(Gun gun)
    {
        int numberOfGranades = Random.Range(minGranadesNumber, maxGranadesNumber);
        gun.ReceiveGranades(numberOfGranades);
    }
}