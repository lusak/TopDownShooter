﻿using UnityEngine;
using System.Collections;

public class ExtraDamagePickup : Pickup
{
    [SerializeField] private int minDurationOfPickup = 4;
    [SerializeField] private int maxDurationOfPickup = 9;

    [SerializeField] private Bullet upgradedBulletsPrefab;

    public override void GrantBonus(Gun gun)
    {
        int effectDuration = Random.Range(minDurationOfPickup, maxDurationOfPickup);
        ExtraDamageInfo extraDamageInfo = FindObjectOfType<ExtraDamageInfo>();
        if(extraDamageInfo)
        {
            extraDamageInfo.DisplayExtraDmgInfo(upgradedBulletsPrefab.VulnerableEnemy, effectDuration);
        }
        gun.ChangeBullet(upgradedBulletsPrefab, effectDuration);
    }
}