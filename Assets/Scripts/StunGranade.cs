﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunGranade : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float explosionRadious = 2f;
    [SerializeField] private float explosionDuration = 0.4f;
    [SerializeField] private float stunDuration = 2f;
    [SerializeField] private AudioClip fireClip;
    [SerializeField] private AudioClip explodeClip;

    private Rigidbody2D rb;

    private void Awake() => rb = GetComponent<Rigidbody2D>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Enemy>())
        {
            StartCoroutine(Explode());
        }
    }

    public void Fire()
    {
        SoundManager.instance.PlayAudio(fireClip, 0.7f);
        rb.velocity = transform.up * speed;
    }

    private IEnumerator Explode()
    {
        rb.velocity = Vector2.zero;
        LayerMask mask = LayerMask.GetMask("Enemies");
        Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, explosionRadious, mask);
        foreach (Collider2D enemyCollider in enemies)
        {
            enemyCollider.gameObject.GetComponent<Enemy>().TriggerStun(stunDuration);
        }
        GetComponent<Animator>().SetTrigger("explode");
        SoundManager.instance.PlayAudio(explodeClip, 1f);
        yield return new WaitForSeconds(explosionDuration);
        Destroy(gameObject);
    }
}
