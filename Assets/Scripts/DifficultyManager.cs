﻿using UnityEngine;
using System.Collections;


public class DifficultyManager 
{
    public static Difficulty difficulty = Difficulty.Normal;
}

public enum Difficulty
{
    Normal,
    Hard,
    Brutal
}