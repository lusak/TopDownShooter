﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

public static class HighscoreSystem
{
    public const string HIGHSCORE_FILE_NAME = "Highscore.hs";
    public static void AddHighscore(int highscoreToAdd)
    {
        
        Highscores highscores = GetHighscores();
        if (highscores == null)
        {
            Highscores newHighscoreFile = new Highscores();
            newHighscoreFile.AddNewHighscore(highscoreToAdd);
            UpdateHighscoreFile(newHighscoreFile);
            return;
        }

        if (highscores.IsNewHighscore(highscoreToAdd))
        {
            highscores.AddNewHighscore(highscoreToAdd);
            UpdateHighscoreFile(highscores);
            return;

        } else
        {
            Debug.LogError("Couldn't add highscore");
        }
    }

    //Creates or updates highscore file
    public static void UpdateHighscoreFile(Highscores highscores)
    {
        try
        {
            BinaryFormatter formatter = new BinaryFormatter();
            string path = Application.persistentDataPath + "/" + HIGHSCORE_FILE_NAME;
            FileStream stream = new FileStream(path, FileMode.Create);

            formatter.Serialize(stream, highscores);
            stream.Close();
        } catch (System.Exception e)
        {
            Debug.LogError("Couldn't create highscore file: " + e.ToString());
        }

    }

    public static Highscores GetHighscores()
    {
        string path = Application.persistentDataPath + "/" + HIGHSCORE_FILE_NAME;
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Highscores highscores = formatter.Deserialize(stream) as Highscores;
            stream.Close();

            return highscores;
        }
        else
        {
            Debug.LogError("Highscore file not found " + path);
            return null;
        }
    }

    public static int[] GetHighscoresSortedArray()
    {
        string path = Application.persistentDataPath + "/" + HIGHSCORE_FILE_NAME;
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Highscores highscores = formatter.Deserialize(stream) as Highscores;
            stream.Close();

            List<int> highscoresList = highscores.HighscoreList; ;
            highscoresList.Sort();
            highscoresList.Reverse();

            return highscoresList.ToArray();
        }
        else
        {
            Debug.LogError("Highscore file not found " + path);
            return null;
        }
    }

    public static int GetHighestScore()
    {
        string path = Application.persistentDataPath + "/" + HIGHSCORE_FILE_NAME;
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Highscores highscores = formatter.Deserialize(stream) as Highscores;
            stream.Close();

            List<int> highscoresList = highscores.HighscoreList; ;

            if(highscoresList.Count > 0)
            {
                highscoresList.Sort();
                highscoresList.Reverse();
                return highscoresList[0];
            }

            return 0;
        }
        else
        {
            Debug.LogError("Highscore file not found " + path);
            return 0;
        }
    }
}
