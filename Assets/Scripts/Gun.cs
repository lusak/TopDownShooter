﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    [SerializeField] private Bullet defaultBulletPrefab;
    [SerializeField] private StunGranade defaultGranadePrefab;

    //Object to help keep hierarchy clean
    [SerializeField] private GameObject parentObjectOfBullets;
    [SerializeField] private NumericTextObject ammoCounter;
    [SerializeField] private NumericTextObject granadeCounter;

    [SerializeField] private int startAmmo = 50;
    [SerializeField] private int startGranades = 1;

    public int CurrentAmmo { get; set; }
    public int CurrentGranades { get; set; }

    private Bullet activeBulletPrefab;
    private bool active = true;
    private IEnumerator lastBulletChangeCoroutine;

    void Start()
    {
        if (!parentObjectOfBullets || !defaultBulletPrefab || !defaultGranadePrefab)
        {
            Debug.LogError("Parent object for bullets, bulletprefab or granadeprefab not set");
        }

        CurrentAmmo = startAmmo;
        CurrentGranades = startGranades;
        ammoCounter.UpdateText(CurrentAmmo);
        granadeCounter.UpdateText(CurrentGranades);
        activeBulletPrefab = defaultBulletPrefab;

        PauseMenu.instance.onGamePaused += OnGamePaused;
        PauseMenu.instance.onGameUnpaused += OnGameUnpaused;
    }

    void Update()
    {
        if(active)
        {
            if (Input.GetMouseButtonDown(0) && CurrentAmmo > 0)
            {
                Shoot();
            }

            if (Input.GetMouseButtonDown(1) && CurrentGranades > 0)
            {
                ShootGranade();
            }
        }
    }

    private void OnDestroy()
    {
        PauseMenu.instance.onGamePaused -= OnGamePaused;
        PauseMenu.instance.onGameUnpaused -= OnGameUnpaused;
    }

    public void ChangeBullet(Bullet newBullet, float time)
    {
        if(lastBulletChangeCoroutine != null)
        {
            StopCoroutine(lastBulletChangeCoroutine);
        }
        lastBulletChangeCoroutine = BulletUpgradeEvent(newBullet, time);
        StartCoroutine(lastBulletChangeCoroutine);
    }

    public void ReceiveAmmo(int value)
    {
        CurrentAmmo += value;
        ammoCounter.UpdateText(CurrentAmmo);
    }

    public void ReceiveGranades(int value)
    {
        CurrentGranades += value;
        granadeCounter.UpdateText(CurrentGranades);
    }

    private IEnumerator BulletUpgradeEvent(Bullet newBullet, float time)
    {
        activeBulletPrefab = newBullet;
        yield return new WaitForSeconds(time);
        activeBulletPrefab = defaultBulletPrefab;
    }

    private void OnGamePaused() => active = false;

    private void OnGameUnpaused() => active = true;


    private void Shoot()
    {
        Bullet bullet = Instantiate(activeBulletPrefab, transform.position, transform.rotation, parentObjectOfBullets.transform);
        bullet.Fire();
        ammoCounter.UpdateText(--CurrentAmmo);
    }

    private void ShootGranade()
    {
        StunGranade granade = Instantiate(defaultGranadePrefab, transform.position, transform.rotation, parentObjectOfBullets.transform);
        granade.Fire();
        granadeCounter.UpdateText(--CurrentGranades);
    }
}
