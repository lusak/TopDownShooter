﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    [SerializeField] private NumericTextObject highestScoreObject;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (this != instance)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        PauseMenu.instance.GameOver = false;
        if (!highestScoreObject)
        {
            Debug.LogError("HighestScoreObject not set for levelManager");
        }
        else
        {
            highestScoreObject.UpdateText(HighscoreSystem.GetHighestScore());
        }
        GameScore.instance.Score = 0;      
    }

    public void GameOver()
    {
        HighscoreSystem.AddHighscore(GameScore.instance.Score);
        PauseMenu.instance.ShowGameOverMenu();
    }
}
