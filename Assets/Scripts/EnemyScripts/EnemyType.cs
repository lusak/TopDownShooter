﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public enum EnemyType
{
    //None is not used by enemy, but used by default bullets that
    //don't deal additional dmg to any enemy
    None,
    Ogre,
    WolfBeast
}