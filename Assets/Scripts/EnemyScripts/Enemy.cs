﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed = 0.1f;
    [SerializeField] private int damage = 20;
    [SerializeField] private int scorePoints = 50;
    [SerializeField] private EnemyType enemyType;

    private PlayerController player;
    private Health health;
    private Rigidbody2D rBody;
    private Animator animator;

    private IEnumerator lastStunCoroutine;

    private bool active = true;

    private void Awake()
    {
        player = FindObjectOfType<PlayerController>();
        health = GetComponent<Health>();
        rBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    void Start()
    {
        health.onDeathEvent += OnDeath;
    }

    void Update()
    {
        if(active)
        {
            RotateTowardsPlayer();
            Move();
        }
    }

    private void OnDestroy()
    {
        health.onDeathEvent -= OnDeath;
    }

    public void OnDeath()
    {
        EnemySpawner.NumberOfActiveEnemies--;
        GameScore.instance.Score += scorePoints;
        Destroy(gameObject);
    }

    public void TriggerStun(float time)
    {
        if(lastStunCoroutine != null)
        {
            StopCoroutine(lastStunCoroutine);
        }
        lastStunCoroutine = Stun(time);
        StartCoroutine(lastStunCoroutine);
    }

    public EnemyType EnemyType 
    { 
          get
        {
            return enemyType;
        }  
    
    }

    public int Damage
    {
        get
        {
            return damage;
        }
    }

    public int ScorePoints
    {
        get
        {
            return scorePoints;
        }
    }

    private IEnumerator Stun(float time)
    {
        active = false;
        animator.SetBool("stunned", true);
        yield return new WaitForSeconds(time);
        animator.SetBool("stunned", false);
        active = true;
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
    }


    private void RotateTowardsPlayer()
    {
        Vector2 direction = new Vector2(player.transform.position.x - transform.position.x, 
            player.transform.position.y - transform.position.y);
        transform.up = direction;
    }
}
