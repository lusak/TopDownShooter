﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //Playable area is 9 units wide (times two) and 5 high (times two).
    //We want enemies to spawn beyond playable are, thus below values are bigger by 1 unit.

    //Width of the playable area in world units + 1
    [SerializeField] private int xPosBeyondPlayableArea = 10;
    //Width of the playable area in world units + 1
    [SerializeField] private int yPosBeyondPlayableArea = 6;

    [SerializeField] private List<Enemy> typesOfEnemies;
    [SerializeField] private int maxNumberOfEnemies = 12;

    [SerializeField] private float normalTimeBetweenSpawns = 8f;
    [SerializeField] private float hardTimeBetweenSpawns = 4f;
    [SerializeField] private float hardcoreTimeBetweenSpawns = 1f;

    //Object to help keep hierarchy clean
    [SerializeField] private GameObject parentObjectOfEnemies;

    private float timeBetweenEnemiesSpawned;
    public static int NumberOfActiveEnemies { get; set; }

    public bool Active { get; set; }
    
    void Start()
    {
        if(!parentObjectOfEnemies)
        {
            Debug.LogError("No parent object for enemies set");
        }

        if(typesOfEnemies == null || typesOfEnemies.Count == 0)
        {
            Debug.LogError("No enemies set for enemy spawner");
        } else
        {
            SetDifficulty();
            NumberOfActiveEnemies = FindObjectsOfType<Enemy>().Length;
            Active = true;
            StartCoroutine(SpawnEnemies());
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (Active)
        {
            int enemyType;
            Vector2 enemyPosition;
            yield return new WaitForSeconds(timeBetweenEnemiesSpawned);

            if (NumberOfActiveEnemies < maxNumberOfEnemies)
            {
                enemyType = Random.Range(0, typesOfEnemies.Count);
                enemyPosition = PickRandomSpawnPosition();
                Instantiate(typesOfEnemies[enemyType], enemyPosition, Quaternion.identity, parentObjectOfEnemies.transform);
                NumberOfActiveEnemies++;
            }
        }
    }

    private Vector2 PickRandomSpawnPosition()
    {
        //Number representing one of four possible spawn areas
        //top, bottom, right or left of the screen
        int spawnArea = Random.Range(0, 4);
        float spawnXPosition;
        float spawnYPosition;

        Vector2 spawnPostion = new Vector2();

        switch (spawnArea)
        {
            //Top of the screen
            case 0:
                {
                    spawnXPosition = Random.Range(-xPosBeyondPlayableArea, xPosBeyondPlayableArea);
                    spawnYPosition = yPosBeyondPlayableArea;
                    spawnPostion = new Vector2(spawnXPosition, spawnYPosition);
                    break;
                }
            //Bottom of the screen
            case 1:
                {
                    spawnXPosition = Random.Range(-xPosBeyondPlayableArea, xPosBeyondPlayableArea);
                    spawnYPosition = -yPosBeyondPlayableArea;
                    spawnPostion = new Vector2(spawnXPosition, spawnYPosition);
                    break;
                }
            //Right side of the screen
            case 2:
                {
                    spawnXPosition = xPosBeyondPlayableArea;
                    spawnYPosition = Random.Range(-yPosBeyondPlayableArea, yPosBeyondPlayableArea);
                    spawnPostion = new Vector2(spawnXPosition, spawnYPosition);
                    break;
                }
            //Left side of the screen
            case 3:
                {
                    spawnXPosition = -xPosBeyondPlayableArea;
                    spawnYPosition = Random.Range(-yPosBeyondPlayableArea, yPosBeyondPlayableArea);
                    spawnPostion = new Vector2(spawnXPosition, spawnYPosition);
                    break;
                }
            default:
                {
                    return spawnPostion;
                }
        }

        return spawnPostion;
    }

    private void SetDifficulty()
    {
        Difficulty difficulty = DifficultyManager.difficulty;

        switch (difficulty)
        {
            case Difficulty.Normal:
                {
                    timeBetweenEnemiesSpawned = normalTimeBetweenSpawns;
                    break;
                }
            case Difficulty.Hard:
                {
                    timeBetweenEnemiesSpawned = hardTimeBetweenSpawns;
                    break;
                }
            case Difficulty.Brutal:
                {
                    timeBetweenEnemiesSpawned = hardcoreTimeBetweenSpawns;
                    break;
                }
            default:
                {
                    timeBetweenEnemiesSpawned = normalTimeBetweenSpawns;
                    break;
                }
        }
    }
}
