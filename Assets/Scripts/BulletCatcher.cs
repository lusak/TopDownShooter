﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCatcher : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Bullet>())
        {
            Destroy(collision.gameObject);
        } else if (collision.gameObject.GetComponent<StunGranade>())
        {
            Destroy(collision.gameObject);
        }
    }
}
