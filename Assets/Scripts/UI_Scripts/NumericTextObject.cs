﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumericTextObject : MonoBehaviour
{
    [SerializeField] private int maxNumberToDisplay = 9999;
    private TextMeshProUGUI text;
    void Awake() => text = GetComponent<TextMeshProUGUI>();

    public void UpdateText(int value)
    {
        if(value < maxNumberToDisplay)
        {
            text.text = value.ToString();
        } else
        {
            text.text = maxNumberToDisplay.ToString();
        }
    }
}
