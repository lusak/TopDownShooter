﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Slider slider;
    public Gradient gradient;
    public Image fill;

    private void Awake() => slider = GetComponent<Slider>();

    public void TakeDamage(int damage)
    {
        slider.value -= damage;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
