﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu instance;
    
    public const int startMenuSceneIndex = 0;
    public static bool GamePaused = false;

    public GameObject pauseMenuUI;
    public GameObject soundButtonGroup;
    public GameObject mainButtonGroup;
    public GameObject gameOverMenu;

    public event Action onGamePaused;
    public event Action onGameUnpaused;

    private bool gameOver = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } else if (this != instance)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if (pauseMenuUI == null)
        {
            Debug.LogError("No pause menu selected");
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().buildIndex > 0 && !gameOver)
            {
                if (GamePaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }

    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;

        onGamePaused?.Invoke();
    }

    public void Resume()
    {
        mainButtonGroup.SetActive(true);
        soundButtonGroup.SetActive(false);
        pauseMenuUI.SetActive(false);
        gameOverMenu.SetActive(false);
        Time.timeScale = 1f;
        GamePaused = false;

        onGameUnpaused?.Invoke();
    }

    public void ShowGameOverMenu()
    {
        gameOver = true;
        pauseMenuUI.SetActive(true);
        mainButtonGroup.SetActive(false);
        gameOverMenu.SetActive(true);
        Time.timeScale = 0f;
        GamePaused = true;

        onGamePaused?.Invoke();
    }

    public void Retry()
    {
        Resume();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadMainMenu()
    {
        Resume();
        SceneManager.LoadScene(startMenuSceneIndex);
    }

    public void QuitGame() => Application.Quit();

    public bool GameOver
    {
        get
        {
            return gameOver;
        }

        set
        {
            gameOver = value;
        }
    }
}
