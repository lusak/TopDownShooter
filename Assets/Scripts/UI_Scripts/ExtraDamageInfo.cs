﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ExtraDamageInfo : MonoBehaviour
{
    private const string ENEMY_INFO_TEXT = "Extra damage vs ";

    [SerializeField] private TextMeshProUGUI enemyInfo;
    [SerializeField] private TextMeshProUGUI timer;

    private IEnumerator lastCoroutine;

    public void DisplayExtraDmgInfo(EnemyType enemyType, int time)
    {
        enemyInfo.text = ENEMY_INFO_TEXT + enemyType.ToString();
        if(lastCoroutine != null)
        {
            StopCoroutine(lastCoroutine);
        }
        lastCoroutine = ExtraDamage(time);
        StartCoroutine(lastCoroutine);
    }

    private IEnumerator ExtraDamage(int time)
    {
        enemyInfo.gameObject.SetActive(true);
        timer.gameObject.SetActive(true);
        while(time >= 0)
        {
            timer.text = time.ToString();
            time--;
            yield return new WaitForSeconds(1);
        }
        enemyInfo.gameObject.SetActive(false);
        timer.gameObject.SetActive(false);
    }
}
