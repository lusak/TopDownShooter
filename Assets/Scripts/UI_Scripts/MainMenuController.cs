﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public void StartNewGame() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

    public void SetDifficulty(int difficulty)
    {
        switch (difficulty)
        {
            case 0:
                {
                    DifficultyManager.difficulty = Difficulty.Normal;
                    break;
                }
            case 1:
                {
                    DifficultyManager.difficulty = Difficulty.Hard;
                    break;
                }
            case 2:
                {
                    DifficultyManager.difficulty = Difficulty.Brutal;
                    break;
                }
            default:
                {
                    DifficultyManager.difficulty = Difficulty.Normal;
                    break;
                }
        }
    }

    public void QuitGame() => Application.Quit();

}
