﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private AudioClip fireClip;
    [SerializeField] private int damage;
    [SerializeField] private EnemyType vulnerableEnemy = EnemyType.None;
    [SerializeField] private int vulnerableEnemyDmgMultiplier = 2;
    
    private Rigidbody2D rb;

    private void Awake() => rb = GetComponent<Rigidbody2D>();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Health health = collision.gameObject.GetComponent<Health>();
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (health)
        {
            if(enemy)
            {
                if(enemy.EnemyType == vulnerableEnemy)
                {
                    health.TakeDamage(damage * vulnerableEnemyDmgMultiplier);
                } else
                {
                    health.TakeDamage(damage);
                }
            } else
            {
                health.TakeDamage(damage);
            }
            Destroy(gameObject);
        }
    }

    public void Fire()
    {
        SoundManager.instance.PlayAudio(fireClip, 0.7f);
        rb.velocity = transform.up * speed;
    }

    public int Damage
    {
        get
        {
            return damage;
        }
    }

    public EnemyType VulnerableEnemy
    {
        get
        {
            return vulnerableEnemy;
        }
    }
}
