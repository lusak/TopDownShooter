﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class GameScore : MonoBehaviour
{
    public static GameScore instance;

    private int scoreMultiplier = 1;

    [SerializeField] private NumericTextObject scoreText;

    private int score = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value * scoreMultiplier;
            scoreText.UpdateText(value * scoreMultiplier);
        }
    }

    public int ScoreMultiplier { get; set; }
}
