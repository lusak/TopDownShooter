Shoot and Run is a simple Top Down Shooter that I created in just 3 days.

Aim of the game is to kill as many enemies as possible. You can shoot enemies with your pistol or stun them with stun granades.
Different pickups grant you additional ammo, granades or ammo upgrades.
When shoot, pickup crates explode and hurt the enemies. 